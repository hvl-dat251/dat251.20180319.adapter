package dat251.adapter.problem;

public class Main {

	// Unfinished. Not expected to run properly yet, but to throw NullPointerException
	public static void main(String[] args) {
        SocketAdapter sockAdapter = null;  // What to do here?
        Volt v3 = sockAdapter.get3Volt();
        Volt v12 = sockAdapter.get12Volt();
        Volt v120 = sockAdapter.get120Volt();
        System.out.println("v3 volts="+v3.getVolts());
        System.out.println("v12 volts="+v12.getVolts());
        System.out.println("v120 volts="+v120.getVolts());
	}

}
